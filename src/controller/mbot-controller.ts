import { Router } from "express";
import { Motor, Piezo, SensorOption } from "johnny-five";

export const mbotController = Router();

let max_speed_l = 150;
let max_speed_r = 150;

let stdin = process.stdin;
stdin.setRawMode(true);

let motorLeft: Motor;
let motorRight: Motor;

mbotController.get('/music', async (req, resp) => {
    try {
        let piezo = new Piezo(8);
        console.log(req.query.song);
        if (req.query.song === "off") {
            console.log("slt");
            console.log(piezo.isPlaying)
            piezo.noTone();
            piezo.off();
            console.log(piezo.isPlaying)

        }
        if (req.query.song === "sw") {
            piezo.play({
                // song is composed by an array of pairs of notes and beats
                // The first argument is the note (null means "no note")
                // The second argument is the length of time (beat) of the note (or non-note)
                song: [
                    ["A4", 1 / 4],
                    ["A4", 1 / 4],
                    ["A4", 1 / 4],
                    ["F4", 3 / 16],
                    ["C5", 1 / 16],
        
                    ["A4", 1 / 4],
                    ["F4", 3 / 16],
                    ["C5", 1 / 16],
                    ["A4", 1 / 2],
        
                    ["E5", 1 / 4],
                    ["E5", 1 / 4],
                    ["E5", 1 / 4],
                    ["F5", 3 / 16],
                    ["C5", 1 / 16],
        
                    ["A4", 1 / 4],
                    ["F4", 3 / 16],
                    ["C5", 1 / 16],
                    ["A4", 1 / 2],
        
                    ["A5", 1 / 4],
                    ["A4", 3 / 16],
                    ["A4", 1 / 16],
                    ["A5", 1 / 4],
                    ["A4", 3 / 16],
                    ["G5", 1 / 16],
        
                    ["G5", 1 / 12],
                    ["F5", 1 / 12],
                    ["G5", 1 / 12],
                    [null, 1 / 2],
        
                    ["B4", 1 / 8],
                    ["E5", 1 / 4],
                    ["  D5", 3 / 16],
                    ["D5", 1 / 16],
        
                    ["C5", 1 / 12],
                    ["B4", 1 / 12],
                    ["C5", 1 / 12],
                    [null, 1 / 2],
                    ["F4", 1 / 8],
                    ["A4", 1 / 4],
                    ["F4", 3 / 16],
                    ["A4", 1 / 16],
                    ["B4", 1 / 4],
                    ["A4", 1 / 16],
                    ["C5", 1 / 16],
                    ["E5", 1 / 2],
        
                ],
                tempo: 30
            });        
        }
        if (req.query.song === "default") {
            console.log(piezo.isPlaying)

            piezo.play({
                // song is composed by an array of pairs of notes and beats
                // The first argument is the note (null means "no note")
                // The second argument is the length of time (beat) of the note (or non-note)
                song: [
                  ["C4", 1 / 4],
                  ["D4", 1 / 4],
                  ["F4", 1 / 4],
                  ["D4", 1 / 4],
                  ["A4", 1 / 4],
                  [null, 1 / 4],
                  ["A4", 1],
                  ["G4", 1],
                  [null, 1 / 2],
                  ["C4", 1 / 4],
                  ["D4", 1 / 4],
                  ["F4", 1 / 4],
                  ["D4", 1 / 4],
                  ["G4", 1 / 4],
                  [null, 1 / 4],
                  ["G4", 1],
                  ["F4", 1],
                  [null, 1 / 2]
                ],
                tempo: 100
              });

        }
    // Plays the same song with a string representation
    }catch(error) {
        console.log(error);
        resp.status(500).json(error);
    }
});


mbotController.get('/move', async (req, resp) => {
    try {
        motorLeft = new Motor({pins: {pwm: 6, dir: 7}});
        motorRight = new Motor({pins: {pwm: 5, dir: 4}});

        stdin.on('keypress', function(chunk, key) {
            // process the keypresses
            if (key) {
                switch (key.name) {
                    case "up":
                        motorLeft.reverse(max_speed_l);
                        motorRight.forward(max_speed_r);
                        break;
                    case "down":
                        motorRight.reverse(max_speed_r);
                        motorLeft.forward(max_speed_l);
                        break;
                    case "left":
                        motorLeft.forward(max_speed_l);
                        motorRight.forward(max_speed_r);
                        break;
                    case "right":
                        motorRight.reverse(max_speed_r);
                        motorLeft.reverse(max_speed_l);
                        break;
                    case "space":
                        motorLeft.stop();
                        motorRight.stop();
                        break;
                }
            }
        });
    }catch(error) {
        console.log(error);
        resp.status(500).json(error);
    }
});

