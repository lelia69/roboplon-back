import { Router } from "express";
import { Board, Motor, Piezo } from "johnny-five";

let max_speed_l = 150;
let max_speed_r = 150;
let stdin = process.stdin;
stdin.setRawMode(true);

stdin.on('keypress', function(chunk, key) {
    console.log("Keypress controller", key)
    const motorLeft = new Motor({pins: {pwm: 6, dir: 7}});
    const motorRight = new Motor({pins: {pwm: 5, dir: 4}});
    
    // process the keypresses
    if (key) {
        switch (key.name) {
            case "up":
                motorLeft.reverse(max_speed_l);
                motorRight.forward(max_speed_r);
                break;
            case "down":
                motorRight.reverse(max_speed_r);
                motorLeft.forward(max_speed_l);
                break;
            case "left":
                motorLeft.forward(max_speed_l);
                motorRight.forward(max_speed_r);
                break;
            case "right":
                motorRight.reverse(max_speed_r);
                motorLeft.reverse(max_speed_l);
                break;
            case "space":
                motorLeft.stop();
                motorRight.stop();
                break;
        }
    }
});