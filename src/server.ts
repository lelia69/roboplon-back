 import express from 'express';
import { Board, Motor, Piezo } from 'johnny-five';
import './controller/keypress-controller';
import { mbotController } from './controller/mbot-controller';

export const server = express();

const port = process.env.PORT;

const board = new Board({port: process.argv[2]})

server.use(express.json());
server.use('/api/mbot', mbotController)
