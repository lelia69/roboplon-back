import { server } from './server';

export const port = process.env.PORT || 8000;


server.listen(port, ()=> {
    console.log('listening on port '+8000);
});


